
# Présentation générale :

Le but de ce projet est de proposé un jeu vidéo qui serait le fruit d'une hybridation entre un dungeon crawler à l'ancienne et jeu d'énigme de type point and click.

Le setup est fantastique classique avec une ambiance néo-noire : L'environnement de jeu est globalement réaliste, mais infusé d'éléments eux plus surréalistes. Ces éléments doivent influencer l'univers de manière subtile, sans être proéminent d'une manière que décrit parfaitement cette citation de Gandalf (Tolkien) :   
« Un bon magicien n'a pas besoin de faire usage de ses pouvoirs ».

Le jeu vous met dans la peau d'un personnage qui arrive dans un village après une vie plus urbaine. L'environnement apparaît d'abord accueillant et chaleureux. Toutefois, rapidement des dissonances commencent à apparaître dans ce tableau. Ces dissonances sont souvent (mais pas systématiquement) révélées par un personnage « jumeau » du joueur, dont on suit globalement les pas dans l'intrigue ( Inspiration : Le G-man de la saga Half Life).

Au fur et à mesure que le jeu progresse, on passe d'un rôle passif, spectateur, à un rôle plus actif, avec la possibilité notamment d'aider ou de mettre des bâtons dans les roues de ce personnage.
Nos actions auront une influence, parfois drastique sur l'environnement et les personnages. Ainsi la moralité va jouer un rôle important.

# Gameplay :

La principale interface du jeu est la ligne de commande. Il existe un petit nombre de commandes qui permettent d'agir sur l'univers :
- se déplacer
- gérer l'inventaire
- ramasser des objets
- actions
- parler à des personnages.

Le retour se fait essentiellement par du texte.

## Lieux

Les lieux sont décrit d'abord par une somme d'états. Exemple : Température, météo...
Ces états influencent les actions possibles.

Les lieux ont une interface commune (description, se déplacer…) et une interface spécifique (ex : grimper à un arbre).

On passe d'un lieu à un autre avec la commande "aller à" suivi du mot clef permettant de se rendre au lieu.

## Objets

Les objets ont une interface commune (lâcher, ramasser, examiner…) et une interface spécifique (autres actions).
Ils ont aussi leurs états.

## Personnage

De même, les personnages ont des états (malades, fatigués, stressé). En fonction de ses états, un personnage pourra ou non réaliser certaines actions.

La gestion des états est dynamiques pour le joueur (via des jauges par ex pour la fatigue) et statique pour les NPC (change sur action, ou de manière périodique seulement).

Les personnages auront entre autre une jauge d'appréciation du joueur, qui permettront ou non certaines actions, de réveler certaines informations etc…

## Temps

Le jeu admet une gestion du temps basique, basé sur des périodes : Matin, Après-Midi, Nuit.
Il existe un compteur de temps, quand celui-ci arrive à zéro, on passe à la période suivante.

Les environnements changent avec le temps, on ne rencontrera pas les mêmes personnages la nuit que le jour dans les rues du village, par exemple.


# Architecture de projet :

Modèle - Vue - Contrôleur.

Toute entrée est passée au contrôleur, qui est chargé de parser les commandes (appel à une classe parser), ensuite il interroge le modèle, qui s'actualise et et communique à la vue les informations à afficher.
La vue va alors récupérer les textes correspondants auprès d'une classe Broker. Les textes sont stockés au format JSON et chargé au démarrage du programme, elle réalise ensuite l'output.

## Modèle

Tout élément intéractif du jeu est modélisé par une interface **IState** et une classe abstraite **AEntity**.
L'intérêt de l'interface est de fournir rapidement la base à l'écriture des test unitaires.

Toute **Entity** comporte une collection d'états **State**, qui sont définis de la mềme manière par **IState** et **ASTATE**.

Les états sont ensuites déclinées dans des classes fille en fonction du type d'entité.

### Entités

- **Place** : Les lieux ont comme principale particularité d'intégrer une collection de **Transition**, c'est à dire, d'accès à un autre lieux.

- **Transition** : Entité modélisant un passage d'un lieu à un autre, implémente en particulier les conditions de déplacement d'un lieu à l'autre. (par exemple, pour escalader un mur, il faut avoir la compétence et ne pas être fatigué).

- **Character** : Toute action réalisée est le fruit d'un personnage. Les personnages, en plus des stocker leurs états généraux stockent des **abilities**.

- **Item** : Représentant des objets ou éléments du décors. On peut intéragir avec. Les items se trouvent dans un lieu, ou dans l'inventaire du joueur.
Sous classes : **Container**, objets pouvant contenir des objets, **Collectible** : Objets pouvant être pris dans l'inventaire.

- **Global** : Etat général du jeu.

### Etats

- **GlobalState** : États généraux du jeu (gestion du temps temps par exemple).

- **PlaceState** : États des lieux. Classe enfant : **ExteriorPlaceState**,

- **CharacterState** : États d'un joueur.

- **Ability** : Classe fille de CharacterState décrivant des capacités ou des savoirs particuliers.