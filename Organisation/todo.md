# Lucas
- [ ] proposer un squelette général des classes du programme afin de répondre au
cahier des charges
- [ ] écrire la feuille de style des classes (pour que tout le monde utilise le
même style)
- [ ] commencer l'implémemtation des classes

# Julien
- [ ] Proposer ébauche de scénario complet
- [ ] Avancer au maximum le scénario du jeu : intrigues, personnages, lieux 
arbres des possibilités…
- [ ] Faire des schéma arborescent du scénario
- [ ] Aider à l'implémentation des classes

# Etienne
- [ ] Réfléchir à la feuille de style des test unitaires
- [ ] écrire les classes de test en miroir de la création des classes, en veillant
à ce qu'elles répondent au cahier des charges
- [ ] Aider à l'implémentation des classes


# Yoann
- [ ] Réaliser la modélisation UML au fur et à mesure de la description et de
l'écriture des classes
- [ ] Aider à l'implémentation des classes

# Karim
- [ ] Retour d'expérience sur les démos et l'expérience joueur