# Projet Jeu Vidéo

## Build :

Le programme se construit avec make. On nettoie le répertoire de build avec la
commande "make clean".


## Deadline :
- Première démo : lundi 2 mars : On doit avoir terminé les principaux mécanismes
du jeu, le scénario doit ếtre quasiment terminé. La démonstration consiste à
montrer les principaux mécanismes du jeu.
- Seconde : 9 mars : Le jeu doit être **globalement** jouable. on doit montrer
une phase de gameplay.
- Troisième : 16 mars : Fin du projet, rendu du jeu.

## Répartition des tâches :

- Lucas : Conception
- Yoann : Modélisation UML
- Etienne : Tests
- Julien : Scénario
- Karim : Game Design

## Conventions d'écriture des classes :

- Variables et fonctions : camelCase.
- Classes : CamelCase.
- Constantes : SNAKE_CASE.
- Tout crochet précédé d'un espace.

- écrire la javadoc pour chaque méthode et attribut (même private).
- Les lignes ne doivent pas dépasser 80 glyphes (colonnes) de long.

## Convention d'écriture des classes de test :

- A ECRIRE

## Informations générales :

- Tout ce qui concerne la conception est dans le répertoire correspondant.
- Idem pour la modélisation UML et le scénario, ainsi que l'organisation
