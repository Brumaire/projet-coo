import org.json.*;

public class Chap1Module {

    private static final Model MODEL = Model.getInstance();
    private static final Player PLAYER = MODEL.getPlayer();
    private static final View VIEW = View.getInstance();

    private static void loadGameData() throws Exception {
        TextBroker tb = TextBroker.getInstance();
        tb.loadTextJSONFile("data/item_01.json");
        tb.loadTextJSONFile("data/container_01.json");
        tb.loadTextJSONFile("data/event_01.json");
        tb.loadTextJSONFile("data/place_01.json");
        tb.loadTextJSONFile("data/chapitre_01.json");
        tb.loadTextJSONFile("data/display_01.json");
        tb.loadTextJSONFile("data/commands_01.json");
        tb.loadTextJSONFile("data/targets_01.json");
    }

    private static void loadDemoGameModel() {

        Character toto = new Character("TOTO");
        Character franck = new Character("VOISIN");
        Character boulanger = new Character("BOULANGER");
        Character epicier = new Character("EPICIER");

        franck.addLine("DEFAULT");
        franck.addLine("BONJOUR");

        /***
        * DEFINITION DES OBJETS DU NIVEAU
        */
        Container rangement = new Container("AUBERGESALLERANGEMENTS",true);
        Container comptoir = new Container("AUBERGESALLECOMPTOIR",true);
        Container tables = new Container("AUBERGESALLETABLES",true);
        Container etagelit = new Container("AUBERGEETAGELIT",true);
        Container etagearmoir = new Container("AUBERGEETAGEARMOIRE",true);
        Container lampe = new Container("AUBERGEETAGELAMPE",true);
        Container etagecoffre = new Container("AUBERGEETAGECOFFRE",true);
        Container gardemanger = new Container("AUBERGECAVEGARDEMANGER",true);
        Container maincoffre = new Container("AUBERGECHAMBRECOFFRE",true);
        Container mainarmoir = new Container("AUBERGECHAMBREARMOIRE",true);

        Item sram = new Item("RAMS",1,false);
        Item cleauberge = new Item("CLEAUBERGE",1,false);
        Item lit = new Item("AUBERGECHAMBRELIT",1,false);
        Item lanterne = new Item("LANTERNE",5,false);

        /***
        * DEFINITION DES ZONES DU JEU
        */
        Place salle = new Place("AUBERGESALLE");
        Place etage = new Place("AUBERGEETAGE");
        Place cave = new Place("AUBERGECAVE");
        Place chambre = new Place("AUBERGECHAMBRE");
        Place vide = new Place("FININTRO");
        Place carrefour = new Place("CARREFOUR");
        Place voisin = new Place("VOISIN");
        Place boulangerie = new Place("BOULANGERIE");
        Place epicerie = new Place("EPICERIE");


        /***
        * DEFINITION DES EVENEMENTS DU JEU
        */
        Event ontravel = new Event("ONTRAVEL", true) {
            public void onEvent(){}
        };

        Event salleetagetravel = new Event("SALLETOETAGE", true) {
            public void onEvent(){}
        };

        Event etagesalletravel = new Event("ETAGETOSALLE", true) {
            public void onEvent(){}
        };

        Event sallecavetravel = new Event("SALLETOCAVE", true) {
            public void onEvent(){}
        };

        Event cavesalletravel = new Event("CAVETOSALLE", true) {
            public void onEvent(){}
        };

        Event cavechambretravel = new Event("CAVETOCHAMBRE", true) {
            public void onEvent(){}
        };

        Event chambrecavetravel = new Event("CHAMBRETOCAVE", true) {
            public void onEvent(){}
        };

        Event carrefourvoisintravel = new Event("CARREFOURTOVOISIN", true) {
            public void onEvent(){}
        };

        Event carrefourboulangerietravel = new Event("CARREFOURTOBOULANGERIE", true) {
            public void onEvent(){}
        };

        Event carrefourepicerietravel = new Event("CARREFOURTOEPICERIE", true) {
            public void onEvent(){}
        };

        Event voisincarrefourtravel = new Event("ALLTOCARREFOUR", true) {
            public void onEvent(){}
        };

        Event boulangeriecarrefourtravel = new Event("BOULANGERFIRSTQUIT", true) {
            public void onEvent(){}
        };

        Event epicerievidetravel = new Event("EPICERIETOVIDE", true) {
            public void onEvent(){}
        };

        Event sallecarrefourtravel = new Event("SALLETOCARREFOUR", true) {
            public void onEvent(){}
        };

        Event addaubergekey = new Event("CHAMBRETOCAVE", true) {
            public void onEvent(){}
        };

        Event eatsram = new Event("EATRAMS",true) {
            public void onEvent(){
                ((Character) getTarget("MAINTARGET")).addLifePoints(20);
            }
        };

        Event uselanterne = new Event("USELANTERNE",true) {
            public void onEvent(){
              View.getInstance().update("MODEL:GAME_OVER");
            }
        };

        Event takekey = new Event("TAKEKEY",true) {
            public void onEvent(){

            }
        };

        /***
        * DEFINITION DES EVENEMENTS DE L'UTILISATION DES OBJETS
        */
        sram.setOnUseEvent(eatsram);
        lanterne.setOnUseEvent(uselanterne);

        /***
        * DEFINITION DES OBJETS DANS LES ZONES
        */
        salle.addItem(new Container(rangement));
        salle.addItem(new Container(tables));
        salle.addItem(new Container(comptoir));
        comptoir.addItem(new Item(cleauberge));
        comptoir.addItem(new Item(sram));
        etage.addItem(new Container(etagelit));
        etage.addItem(new Container(etagearmoir));
        etage.addItem(new Container(lampe));
        etage.addItem(new Container(etagecoffre));
        cave.addItem(new Container(gardemanger));
        gardemanger.addItem(new Item(sram));
        chambre.addItem(new Item(lit));
        chambre.addItem(new Container(maincoffre));
        maincoffre.addItem(new Item(lanterne));
        chambre.addItem(new Container(mainarmoir));

        /***
        * DEFINITION DES POINTS DE VOYAGE DU JEU
        */
        TravelPoint etagetosalle = new TravelPoint("ETAGESALLETRAVEL",etage,salle,etagesalletravel);
        TravelPoint cavetosalle = new TravelPoint("CAVESALLETRAVEL",cave,salle,cavesalletravel);
        TravelPoint cavetochambre = new TravelPoint("CAVECHAMBRETRAVEL",cave,chambre,cavechambretravel);
        TravelPoint chambretocave = new TravelPoint("CHAMBRECAVETRAVEL",chambre,cave,chambrecavetravel);
        TravelPoint salletoetage = new TravelPoint("SALLEETAGETRAVEL",salle,etage,salleetagetravel);
        TravelPoint salletocave = new TravelPoint("SALLECAVETRAVEL",salle,cave,sallecavetravel);
        TravelPoint salletocarrefour = new TravelPoint("SALLECARREFOURTRAVEL",salle,carrefour,sallecarrefourtravel);
        TravelPoint carrefourtovoisin = new TravelPoint("CARREFOURVOISINTRAVEL",carrefour,voisin,carrefourvoisintravel);
        TravelPoint carrefourtoboulangerie = new TravelPoint("CARREFOURBOULANGERIETRAVEL",carrefour,boulangerie,carrefourboulangerietravel);
        TravelPoint carrefourtoepicerie = new TravelPoint("CARREFOUREPICERIETRAVEL",carrefour,epicerie,carrefourepicerietravel);
        TravelPoint voisintocarrefour = new TravelPoint("VOISINCARREFOURTRAVEL",voisin,carrefour,voisincarrefourtravel);
        TravelPoint boulangerietocarrefour = new TravelPoint("BOULANGERIECARREFOURTRAVEL",boulangerie,carrefour,boulangeriecarrefourtravel);
        TravelPoint epicerietocarrefour = new TravelPoint("EPICERIECARREFOURTRAVEL",epicerie,vide,epicerievidetravel);

        MODEL.forcePlayerLocation(chambre);
    }

	public static void main(String[] args) throws Exception {
        Chap1Module.loadGameData();
        Chap1Module.loadDemoGameModel();

        Controller.getInstance().Launcher("MAIN:INTRO");
	}
}
