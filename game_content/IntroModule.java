import org.json.*;

public class IntroModule {

    private static final Model MODEL = Model.getInstance();
    private static final Player PLAYER = MODEL.getPlayer();
    private static final View VIEW = View.getInstance();

    private static void loadGameData() throws Exception {
        TextBroker tb = TextBroker.getInstance();
        tb.loadTextJSONFile("data/item_01.json");
        tb.loadTextJSONFile("data/container_01.json");
        tb.loadTextJSONFile("data/event_01.json");
        tb.loadTextJSONFile("data/place_01.json");
        tb.loadTextJSONFile("data/intro_01.json");
        tb.loadTextJSONFile("data/display_01.json");
        tb.loadTextJSONFile("data/commands_01.json");
        tb.loadTextJSONFile("data/targets_01.json");
    }

    private static void loadDemoGameModel() {

        Character toto = new Character("TOTO");

        //StateBroker.getInstance().addState("LOCKED");
        /***
        * DEFINITION DES OBJETS DU NIVEAU
        */
        Container rangement = new Container("AUBERGESALLERANGEMENTS",true);
        Container comptoir = new Container("AUBERGESALLECOMPTOIR",true);
        Container tables = new Container("AUBERGESALLETABLES",true);
        Container etagelit = new Container("AUBERGEETAGELIT",true);
        Container etagearmoir = new Container("AUBERGEETAGEARMOIRE",true);
        Container lampe = new Container("AUBERGEETAGELAMPE",true);
        Container etagecoffre = new Container("AUBERGEETAGECOFFRE",true);
        Container gardemanger = new Container("AUBERGECAVEGARDEMANGER",true);
        Container maincoffre = new Container("AUBERGECHAMBRECOFFRE",true);
        Container mainarmoir = new Container("AUBERGECHAMBREARMOIRE",true);

        Item sram = new Item("RAMS",1,false);
        Item cleauberge = new Item("CLEAUBERGE",1,false);
        Item lit = new Item("AUBERGECHAMBRELIT",1,false);
        Item lanterne = new Item("LANTERNE",5,false);


        /***
        * DEFINITION DES ZONES DU JEU
        */
        Place village = new Place("VILLAGEINTRO");
        Place auberge = new Place("AUBERGEINTRO");
        Place salle = new Place("AUBERGESALLE");
        Place etage = new Place("AUBERGEETAGE");
        Place cave = new Place("AUBERGECAVE");
        Place chambre = new Place("AUBERGECHAMBRE");
        Place vide = new Place("FININTRO");
        /***
        * DEFINITION DES EVENEMENTS DU JEU
        */
        Event ontravel = new Event("ONTRAVEL", true) {
            public void onEvent(){}
        };

        Event firstenter = new Event("FIRSTENTER", true) {
            public void onEvent(){}
        };

        Event salleetagetravel = new Event("SALLETOETAGE", true) {
            public void onEvent(){

            }
        };

        Event etagesalletravel = new Event("ETAGETOSALLE", true) {
            public void onEvent(){}
        };

        Event sallecavetravel = new Event("SALLETOCAVE", true) {
            public void onEvent(){}
        };

        Event cavesalletravel = new Event("CAVETOSALLE", true) {
            public void onEvent(){}
        };

        Event cavechambretravel = new Event("CAVETOCHAMBRE", true) {
            public void onEvent(){}
        };

        Event chambrecavetravel = new Event("CHAMBRETOCAVE", true) {
            public void onEvent(){}
        };

        Event addaubergekey = new Event("CHAMBRETOCAVE", true) {
            public void onEvent(){}
        };

        Event eatsram = new Event("EATRAMS",true) {
            public void onEvent(){
                ((Character) getTarget("MAINTARGET")).addLifePoints(20);
            }
        };

        Event usekey = new Event("USEKEY",true) {
            public void onEvent(){
              TravelPoint salletoetage = new TravelPoint("SALLEETAGETRAVEL",salle,etage,salleetagetravel);
              TravelPoint salletocave = new TravelPoint("SALLECAVETRAVEL",salle,cave,sallecavetravel);
            }
        };

        Event uselanterne = new Event("USELANTERNE",true) {
            public void onEvent(){
              View.getInstance().update("MODEL:GAME_OVER");
            }
        };

        Event gotobed = new Event("UTILISELIT",true) {
            public void onEvent(){
              //changement de chapitre
              MODEL.forcePlayerLocation(vide);
            }
        };

        /***
        * DEFINITION DES EVENEMENTS DE L'UTILISATION DES OBJETS
        */
        sram.setOnUseEvent(eatsram);
        cleauberge.setOnTakeEvent(usekey);
        lit.setOnUseEvent(gotobed);
        lanterne.setOnUseEvent(uselanterne);
        /***
        * DEFINITION DES OBJETS DANS LES ZONES
        */
        salle.addItem(new Container(rangement));
        salle.addItem(new Container(tables));
        salle.addItem(new Container(comptoir));
        //salle.addItem(new Item(cleauberge));
        //salle.addItem(new Item(sram));

        comptoir.addItem(new Item(cleauberge));
        comptoir.addItem(new Item(sram));
        etage.addItem(new Container(etagelit));
        etage.addItem(new Container(etagearmoir));
        etage.addItem(new Container(lampe));
        etage.addItem(new Container(etagecoffre));
        cave.addItem(new Container(gardemanger));
        gardemanger.addItem(new Item(sram));
        chambre.addItem(new Item(lit));
        chambre.addItem(new Container(maincoffre));
        maincoffre.addItem(new Item(lanterne));
        chambre.addItem(new Container(mainarmoir));

        /***
        * DEFINITION DES POINTS DE VOYAGE DU JEU
        */
        TravelPoint tp = new TravelPoint("VOIDTRAVEL",village,auberge,ontravel);
        TravelPoint enter = new TravelPoint("AUBERGETRAVEL",auberge,salle,firstenter);
        TravelPoint etagetosalle = new TravelPoint("ETAGESALLETRAVEL",etage,salle,etagesalletravel);
        TravelPoint cavetosalle = new TravelPoint("CAVESALLETRAVEL",cave,salle,cavesalletravel);
        TravelPoint cavetochambre = new TravelPoint("CAVECHAMBRETRAVEL",cave,chambre,cavechambretravel);
        TravelPoint chambretocave = new TravelPoint("CHAMBRECAVETRAVEL",chambre,cave,chambrecavetravel);
        //TravelPoint salletoetage = new TravelPoint("SALLEETAGETRAVEL",salle,etage,salleetagetravel);
        //TravelPoint salletocave = new TravelPoint("SALLECAVETRAVEL",salle,cave,sallecavetravel);

        //salletoetage.addState("LOCKED");
        //salletocave.addState("LOCKED");

        MODEL.forcePlayerLocation(auberge);
    }

	public static void main(String[] args) throws Exception {
        IntroModule.loadGameData();
        IntroModule.loadDemoGameModel();
		
		Controller.getInstance().Launcher("MAIN:INTRO");
	}
}
