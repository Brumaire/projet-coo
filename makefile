

JFLAGS = -g
JC = javac

DFLAG = -d
DEST = ./build/

SRCFLAG = -sourcepath
SRC = ./src/
GAME = ./game_content/
SRCI = ./src/interfaces/
SRCE = ./src/exceptions/
SRCC = ./src/Command/
SRCS = ./src/display_strategies/
SRCALL = $(SRC):$(SRCI):$(SRCE):$(SRCC):$(SRCS)

CPFLAG = -cp
CP = .:./build/

DEPS = ./deps/
DATA = ./data/


.SUFFIXES: .java .class



all: clean dependencies interfaces commands strategies
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(SRC)*.java
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(GAME)*.java

dependencies:
	unzip $(DEPS)*.jar -d $(DEST) && rm -rf $(DEST)META-INF
	cp -r $(DATA) $(DEST)

commands:
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(SRCC)*.java

strategies:
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(SRCS)*.java


exceptions:
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(SRCE)*.java

interfaces:
	$(JC) $(CPFLAG) $(CP) $(SRCFLAG) $(SRCALL) $(JFLAGS) $(DFLAG) $(DEST) $(SRCI)*.java


clean:
	rm -rf $(DEST)*.class
	rm -rf $(DEST)org
	rm -rf $(DEST)$(DATA)
