import java.util.*;

public class Character extends Entity
                       implements IInventory, ICharacter {
    
    public static final String CLASS_ID = "CHARACTER";

	private int friendship;
	private int lifePoints;
	private List<Item> inventory;
	
	public Character(String id) {
		super(CLASS_ID,id);
		this.friendship = 0;
		this.lifePoints = 100;
        this.inventory = new ArrayList<Item>();
        this.createDefaultLine();
    }
	
	public Character(String id, int friendship) {
		super(CLASS_ID,id);
		setFriendship(friendship);
		this.lifePoints = 100;
        this.inventory = new ArrayList<Item>();
        this.createDefaultLine();
}
	public Character(String id,  int friendship, int life) {
		super(CLASS_ID,id);
		setFriendship(friendship);
		setLifePoints(life);
        this.inventory = new ArrayList<Item>();
        this.createDefaultLine();
	}

    private void createDefaultLine() {
        String dLineId = this.getId() + "_" + "DEFAULT";
        StateBroker.getInstance().addState(dLineId,"LINE");
        this.addLine(dLineId);
    }
	
	public int getFriendship() {
		return this.friendship;
	}
	
	public void setFriendship(int friendly) {
		this.friendship = friendly;
		if(this.friendship > 10)
			this.friendship = 10;
		if(this.friendship < -10)
			this.friendship = -10; 	
	}
	
	public int getLifePoints() {
		return this.lifePoints;
	}
	
	public void setLifePoints(int life) {
		this.lifePoints = life;
		if(this.lifePoints > 100)
			this.lifePoints = 100;
	}

    public void addLifePoints(int life) {
        this.setLifePoints
            ( this.getLifePoints() + life );
    }
	
	/**********************
	*	implements ICharacters
	*
	**********************/
	
	public boolean travel(TravelPoint tp){
		boolean travelled = false;
		if(tp != null && tp.fromPlace().hasCharacter(this)){
			tp.onTravel(this);
            tp.fromPlace().delCharacter(this);
            tp.toPlace().addCharacter(this);
            travelled = true;
        }
        return travelled;
	}
	
	public boolean takeItem(IInventory from, Item i){
	    boolean result = false;
        // On peut prendre si l'item existe dans l'inventaire et si ce n'est pas
        // un prop (décoration de lieu).
        if(from.hasItem(i) && ! i.isProp()){
            from.putItem(i, this);
            i.onTake(this);
            result = true;
        }
        return result;
	}
	
	public boolean useItem(IInventory from, Item i){
		boolean result = false;
        if(from.hasItem(i)){
			i.onUse(this);
            result = true;
            if(i.getDurability() == 0){
                from.delItem(i);
            }
        }
	    return result;
    }
	
	public int addLine(String lineId){
	    return this.addState(lineId);
	}
	
	public int delLine(String lineId){
	    return this.delState(lineId);
	}
	
	public String talk(Character c, String keyword){
	    String lineIdent = c.getId() + "_" + keyword.toUpperCase();
        Line l = null;
        if(c.hasState(lineIdent)){
            l = (Line) StateBroker.getInstance().getState(lineIdent);
        }
        else {
            l = (Line) StateBroker.getInstance().getState
                (c.getId() + "_" + "DEFAULT");

        }
        l.onTalk(this);
        return l.getTextId();
	}
	/**********************
	*	implements IInventory
	*
	**********************/
	
	public boolean hasItem(Item toSearch){
        boolean result = false;
        Iterator<Item> i = this.inventory.iterator();
        while (!result && i.hasNext() ){
            Item item = i.next();
            if(item.getId() == toSearch.getId()){
                result = true;
            }
        }
        return result;
    }
    public boolean hasItem(String itemID){
        boolean result = false;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = true;
            }
        }
        return result;
    }

    public Item getItem(String itemID){
        Item result = null;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = i;
            }
        }
        return result;
    }
    public void addItem(Item toAdd){
        this.inventory.add(toAdd);
    }

    public boolean delItem(Item item){
        boolean found = false;
        Iterator<Item> i = this.inventory.iterator();
        while ( !found && i.hasNext() ){
            Item item2 = i.next();
            if( item2 == item ){
                i.remove();
                found = true;
            }
        }
        return found;
    }

    public void putItem(Item i, IInventory to){
        if( this.delItem(i) ) to.addItem(i);
    }

    public Item[] getItems(){
        Item[] tab = new Item[this.inventory.size()];
        tab = this.inventory.toArray(tab);
        return tab;
    }
}
