
public class DisplayInventoryCommand implements ICommand{
	
	public DisplayInventoryCommand(){
	}
	
	public boolean execute(){
		View.getInstance().update("CONTROLLER:SHOW_PLAYER_INVENTORY");
        return true;
	}
}
