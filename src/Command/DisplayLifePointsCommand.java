
public class DisplayLifePointsCommand implements ICommand{
	
	public DisplayLifePointsCommand(){
	}
	
	public boolean execute(){
		View.getInstance().update("CONTROLLER:SHOW_PLAYER_LIFEPOINTS");
        return true;
	}
}
