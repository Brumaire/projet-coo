
public class GoBackCommand implements ICommand{

	public boolean execute(){
		View.getInstance().update("CONTROLLER:PREVIOUS_VIEW");	
		return true;
	}
}
