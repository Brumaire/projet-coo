

public class InspectCommand implements ICommand{

    private final Model MODEL = Model.getInstance();
	private final String entity;
	
    public InspectCommand(){
        entity = "";
    }

	public InspectCommand(String entity){
		this.entity = TargetLinker.getInstance().getTarget(entity);
	}
	
	public boolean execute(){
        Place playerLocation = MODEL.getPlayerLocation();
		boolean executed = false;
		if(this.entity.equals("")) {
            MODEL.setEntity(playerLocation);
            executed = true;
        }
        else if(playerLocation.hasItem(entity)) {
                MODEL.setEntity(playerLocation.getItem(entity));
                executed = true;
        }
        else if(playerLocation.hasCharacter(entity)) {
                MODEL.setEntity(playerLocation.getCharacter(entity));
                executed = true;
        }

        if(executed) View.getInstance().update("CONTROLLER:INSPECT");

        return executed;	
	}	
}
