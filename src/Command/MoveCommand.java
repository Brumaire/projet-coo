

public class MoveCommand implements ICommand{

	private final String destination;
	
	public MoveCommand(String destination){
		this.destination = TargetLinker.getInstance().getTarget(destination);
	}
	
	
	public boolean execute(){
		return Model.getInstance().playerMove(this.destination);
	}	
}
