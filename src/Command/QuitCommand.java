
public class QuitCommand implements ICommand{

	public boolean execute(){
		View.getInstance().update("CONTROLLER:QUIT");	
		return true;
	}
}
