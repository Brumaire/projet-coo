

public class TakeItemCommand implements ICommand{

	private final String item;
	
	public TakeItemCommand(String item){
		this.item = TargetLinker.getInstance().getTarget(item);
	}
	
	public boolean execute(){
		
		return Model.getInstance().playerTakeItemFromPlace(this.item);
	}	
}
