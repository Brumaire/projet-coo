

public class TalkCommand implements ICommand{

	private String c;
	private String keyword;
	
	public TalkCommand(String c, String keyword){
		this.c = TargetLinker.getInstance().getTarget(c);
		if (keyword != "") this.keyword = keyword;
        else this.keyword = "DEFAULT";
	}
	
	public boolean execute(){

		return Model.getInstance().playerTalk(this.c, this.keyword);
	}	
}
