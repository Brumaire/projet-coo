

public class UnknownCommand implements ICommand{

	public boolean execute(){
		View.getInstance().update("CONTROLLER:UNKNOWN_COMMAND");
		return true;
	}
}
