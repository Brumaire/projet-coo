

public class UseItemCommand implements ICommand{

	private final String item;
	
	public UseItemCommand(String item){
		this.item = TargetLinker.getInstance().getTarget(item);
	}
	
	public boolean execute(){
		boolean executed = false;
		if(Model.getInstance().playerUseItemFromPlace(this.item)){
            executed = true;
        }
        else if(Model.getInstance().playerUseItemFromInventory(this.item)){
            executed = true;
        }
		return executed;	
	}	
}
