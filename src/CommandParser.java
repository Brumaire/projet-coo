import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de splitter une commande
 **/
public class CommandParser {
    private final List<String> args = new ArrayList<String>();
    private String keyword;

    private void tokenize(String str){

        String[] tokens = str.split(" ");
        this.keyword = tokens[0];
        for(int i = 1; i < tokens.length; i++){
            this.args.add(tokens[i]);
        }

    }

    public CommandParser(String command){
        command = commandNameStandardization(command);
        this.tokenize(command);
    }

    public String getKeyword(){
        return this.keyword;
    }

    public String[] getArgs(){
        return (String[])this.args.toArray();
    }

    public int getArity(){
        return this.args.size();
    }

    public String getArg(int num){
        if(this.args.size() > num)
            return this.args.get(num);
        else return "";
    }

	/*
    *	Methode formattant une chaine de caractere selon la norme de command
    */
    public static String commandNameStandardization(String commandName){
    	return commandName.trim().replaceAll("\\s+", " ").toLowerCase();
    } 


}
