import java.util.*;

public class Container extends Item 
                       implements IInventory {

	private List<Item> inventory;
	private int maxItem;
	private static final int MAXITEM_DEFAULT = 3;
	private static final String CLASSID = "CONTAINER";
	private static final boolean PROP_DEFAULT = true;

	public Container(Container c) {
	    super(c);
        super.classId = CLASSID;
	    this.inventory = c.getInventory();
	    this.maxItem = c.getMaxItem();
	}
	
	/*
	*	Constructeur d'une containeur par défaut indestructible
	*	et pas prop 
	*/
	public Container(String id){
		super(id, -1,  PROP_DEFAULT);
        this.classId = CLASSID;
		this.inventory = new ArrayList<Item>();
		this.maxItem = MAXITEM_DEFAULT;
	}
	
	public Container(String id, boolean prop){
		super(id, -1, prop);
        this.classId = CLASSID;
		this.inventory = new ArrayList<Item>();
		this.maxItem = MAXITEM_DEFAULT;
	}
	
	public Container(String id, int maxItem){
		super(id, -1,  PROP_DEFAULT);
        this.classId = CLASSID;
		this.inventory = new ArrayList<Item>();
		this.setMaxItem(maxItem);
	}
	
	public Container(String id, boolean prop, int maxItem){
		super(id, -1, prop);

        this.classId = CLASSID;
		this.inventory = new ArrayList<Item>();
		this.setMaxItem(maxItem);
	}
	public boolean hasItem(Item toSearch){
        boolean result = false;
        Iterator<Item> i = this.inventory.iterator();
        while (!result && i.hasNext() ){
            Item item = i.next();
            if(item.getId() == toSearch.getId()){
                result = true;
            }
        }
        return result;
    }

	public boolean hasItem(String itemID){
        boolean result = false;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = true;
            }
        }
        return result;
    }

    public Item getItem(String itemID){
        Item result = null;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = i;
            }
        }
        return result;
    }

    public void addItem(Item toAdd){
        this.inventory.add(toAdd);
    }

    public boolean delItem(Item item){
        boolean found = false;
        Iterator<Item> i = this.inventory.iterator();
        while ( !found && i.hasNext() ){
            Item item2 = i.next();
            if( item2 == item ){
                i.remove();
                found = true;
            }
        }
        return found;
    }

    public void putItem(Item i, IInventory to){
        if( this.delItem(i) ) to.addItem(i);
    }

    public Item[] getItems(){
        Item[] tab = new Item[this.inventory.size()];
        tab = this.inventory.toArray(tab);
        return tab;
    }

    public  List<Item> getInventory(){
        return this.inventory;
    }
    
    public int getMaxItem(){
    	return this.maxItem;
    }
    
    public void setMaxItem(int max){
    	if(max < 0)
    		max = 0;
    	this.maxItem = max;
    }

    public String getClassId(){
        return Container.CLASSID;
    }
}
