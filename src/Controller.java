import java.util.*;

public class Controller{

	//Pour waitForCommand
	private final Scanner sc = new Scanner(System.in);
	
	///////////////////////////////////////////////////////////////////////////
    // Singleton
    ///////////////////////////////////////////////////////////////////////////

    private static final Controller SELF = new Controller();

    public static final TextBroker TB = TextBroker.getInstance();

    public static Controller getInstance(){
        return Controller.SELF;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Constantes
    ///////////////////////////////////////////////////////////////////////////

        public static final String QUIT_COMMAND = 
                                  TB.getText("QUIT_COMMAND");
        public static final String GOBACK_COMMAND =
                                  TB.getText("GOBACK_COMMAND");
        public static final String HELP_COMMAND = 
                                  TB.getText("HELP_COMMAND");
        public static final String MOVE_COMMAND = 
                                  TB.getText("MOVE_COMMAND");
        public static final String USE_ITEM_COMMAND = 
                                  TB.getText("USE_ITEM_COMMAND");
        public static final String TAKE_ITEM_COMMAND =
                                  TB.getText("TAKE_ITEM_COMMAND");
        public static final String TALK_COMMAND =
                                  TB.getText("TALK_COMMAND");
        public static final String DISPLAY_INVENTORY_COMMAND =
                                  TB.getText("DISPLAY_INVENTORY_COMMAND");
        public static final String INSPECT_COMMAND =
                                  TB.getText("INSPECT_COMMAND");
        public static final String DISPLAY_LIFEPOINTS_COMMAND =
                                  TB.getText("DISPLAY_LIFEPOINTS_COMMAND");                        

    
    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////

	private final HashMap<String, ICommand> commandMap;
	
	///////////////////////////////////////////////////////////////////////////
    // Constructeurs
    ///////////////////////////////////////////////////////////////////////////

	public Controller(){
		this.commandMap = new HashMap<String, ICommand>();
	}
	
	////////////////////////////////////////////////////////////////////
	///// API COMMAND
	///////////////////////////////////////////////////////////////////
	
	public void command(String commandString) {
		CommandParser cp = new CommandParser(commandString);
    	String cmd = cp.getKeyword();
        int arity = cp.getArity();
    	ICommand command = null;
        
        if(cmd.equals(QUIT_COMMAND)){
            command = new QuitCommand();
        }
        else if(cmd.equals(GOBACK_COMMAND)){
            command = new GoBackCommand();
        }
        else if(cmd.equals(HELP_COMMAND)){
            command = new HelpCommand();
        }
        else if(cmd.equals(MOVE_COMMAND) && arity == 1){
            command = new MoveCommand(cp.getArg(0));
        }
        else if(cmd.equals(USE_ITEM_COMMAND) && arity == 1){
            command = new UseItemCommand(cp.getArg(0));
        }
        else if(cmd.equals(TAKE_ITEM_COMMAND) && arity == 1){
            command = new TakeItemCommand(cp.getArg(0));
        }
        else if(cmd.equals(TALK_COMMAND) && (arity == 1 || arity == 2)){
            command = new TalkCommand(cp.getArg(0),cp.getArg(1));
        }
        else if(cmd.equals(DISPLAY_INVENTORY_COMMAND)){
            command = new DisplayInventoryCommand();
        }
        else if(cmd.equals(DISPLAY_LIFEPOINTS_COMMAND)){
            command = new DisplayLifePointsCommand();
        }
        else if(cmd.equals(INSPECT_COMMAND)){
            command = new InspectCommand(cp.getArg(0));
        }
        else {
            command = new UnknownCommand();
        }


        
        boolean executed = command.execute();
        if(!executed){
            View.getInstance().update("CONTROLLER:BAD_ARGS");           
        }
    }
    
	private void waitForCommand(){
		String command = sc.nextLine();
		this.command(command);
	}
	
	public void Launcher(String view){
		View.getInstance().update(view);
		while(true){
			this.waitForCommand();
		}
	}
	
	public void waitKey(){
		sc.nextLine();
	}
}
