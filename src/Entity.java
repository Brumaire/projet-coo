import java.util.*;

/**
 * Implémentation des entitées.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 **/
public abstract class Entity implements IEntity {
    
    public static final StateBroker SB = StateBroker.getInstance();

    private Set<State> states;
    protected String classId;
    private String id;

    public Entity(String classId, String id){
    	this.classId = classId;
        this.id = id;
        this.states = new HashSet<State>();

    }

    private State getStateFromStateBroker(String stateID){
        return SB.getState(stateID);
    }

    public boolean hasState(String stateID){
        State state = getStateFromStateBroker(stateID);
        return this.states.contains(state);
    }

    public int addState(String stateId){
        State state = getStateFromStateBroker(stateId);
        if (state != null) 
            return ( states.add(state) ? 0 : -1 );
        else return -2;
        
    }
    
    public int delState(String stateID){
        State state = getStateFromStateBroker(stateID);
        if (state != null ) return (this.states.remove(state)) ? 0 : -1;
        else return -2;

    }

    public String getId(){
        return this.id;
    }
    
    public String getClassId(){
        return this.classId;
    }
}
