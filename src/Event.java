import java.util.*;

public class Event implements IEvent{

	private final String id;
	private Map<String, Entity> targets;
    private boolean display;
	
    public Event(String id){
        this.id = id;
        this.targets = new HashMap<String, Entity>();
        this.display = false;

    }
	
	public Event(String id, boolean display){
        this.id = id;
        this.targets = new HashMap<String, Entity>();
        this.display = display; 
	}
	
	public void setTargets(Map<String,Entity> targets){
		this.targets = targets;		
	}
	
	
	public void addTarget(String id, Entity target){
		this.targets.put(id.toUpperCase(), target);
	}
	
	public Entity getTarget(String id){
		return this.targets.get(id.toUpperCase());
	}
	
	public void onEvent(){
        //prévenir le modèle.
	}
	 
	public void run(){
		this.onEvent();
        Model.getInstance().setLastEvent(this);
        if(this.display){
            View.getInstance().update("EVENT:TRIGGERED");
        }
	}

    public String getId(){
        return this.id;
    }
}
