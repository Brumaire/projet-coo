
/**
 * Classe Derivée d'entity permettant de contenir les états concernant
 * l'ensemble du jeu.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 **/
public class Global extends Entity
                    implements ISingleton {

	private static final String CLASS_ID = "Global";
	
    private static final Global SELF = new Global("Global");

    public static Global getInstance(){
        return Global.SELF;
    }

    public Global(String id){
        super(CLASS_ID, id);
    }
}
