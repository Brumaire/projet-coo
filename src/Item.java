
public class Item extends Entity
                  implements IItem {

	private int durability;
	private boolean prop;
	private Event ontake, onuse;
	private final static String CLASS_ID = "ITEM";

    public Item(Item i) {
        super("ITEM", i.getId());
        this.durability = i.getDurability();
        this.prop = i.isProp();
        this.onuse = i.getOnUseEvent();
        this.ontake = i.getOnTakeEvent();
    }

    public Item(String id, int durability,  boolean prop) {
		super("ITEM", id);
        if(durability < -2)
			throw new IllegalArgumentException("Durabilité de l'objet inférieur à -2.");
		this.durability = durability;

        this.prop = prop;
    }
	
	public Item(String id, int durability, Event ontake, Event onuse) {
        super("ITEM", id);
		if(durability < -2)
			throw new IllegalArgumentException("Durabilité de l'objet inférieur à -2.");
		this.durability = durability;
		setOnTakeEvent(ontake);
		setOnUseEvent(onuse);
		this.prop = false;
	}
	
	public Item(String id, int durability, Event onuse, boolean prop) {
		super(CLASS_ID, id);
        this.durability = durability;
		setOnUseEvent(onuse);
		this.prop = prop;
	}
	
	public int getDurability(){
		return this.durability;
	}
	
	public boolean isProp(){
		return this.prop;
	}
	
	public void onUse(Character user){
		if(this.onuse != null  && this.durability != -2 && this.durability != 0) {
            this.onuse.addTarget("mainTarget",user);
			this.onuse.run();
			if(this.durability > 0){
		        this.durability--;
            }
        }
	}
	
	public void onTake(Character taker){
		if(this.ontake != null) { 
            this.ontake.addTarget("mainTarget",taker);
            this.ontake.run();
        }
	}
	
	public boolean setOnUseEvent(Event e){
		this.onuse = e;
        return true; // TODO changer ça
    }
    
	public boolean setOnTakeEvent(Event e){
		this.ontake = e;
        return true; // TODO changer ça.
    }

    public Event getOnUseEvent(){
        return this.onuse;
    }

    public Event getOnTakeEvent(){
        return this.ontake;
    }
}
