public class Line extends State {

    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////

        private Event onTalkEvent;

    ///////////////////////////////////////////////////////////////////////////
    // Constructeurs
    ///////////////////////////////////////////////////////////////////////////

        public Line(String stateID){
            super("LINE",stateID);
        }

        public Line(String classID, String stateID){
            super("LINE", stateID);
        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes publiques
    ///////////////////////////////////////////////////////////////////////////
        
        public void setOnTalkEvent(Event e){
            boolean result = false;
            if(e != null) result = true;
            this.onTalkEvent = e;
            
        }

        public void onTalk(Character talker){
            if(this.onTalkEvent != null){
                this.onTalkEvent.addTarget("mainTarget",talker);
                this.onTalkEvent.run();
            }
        }

        public String getTextId(){
            return (this.getClassId()+"_"+this.getId()).toUpperCase();
        }
}
