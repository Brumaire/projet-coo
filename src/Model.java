import java.util.*;

public class Model implements ISingleton{

    ///////////////////////////////////////////////////////////////////////////
    // Singleton
    ///////////////////////////////////////////////////////////////////////////

        private static final Model SELF = new Model();

        public static Model getInstance(){
            return Model.SELF;
        }
    

    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////
	    private List<IObserver> observers;
    	private Place playerLocation;
	
        private final Player PLAYER;
        private final Global GLOBAL = Global.getInstance();
        
        private Entity entity;
        private Event lastEvent;
	
    ///////////////////////////////////////////////////////////////////////////
    // Contructeurs
    ///////////////////////////////////////////////////////////////////////////

	    public Model() {
		    this.PLAYER = Player.getInstance();
		    this.observers = new ArrayList<IObserver>();
	    }

    ///////////////////////////////////////////////////////////////////////////
    // API pour la gestion des observeurs
    ///////////////////////////////////////////////////////////////////////////

	    public void addObserver(IObserver observer){
		    observers.add(observer);		
	    }

	    public void notifyAllObservers(String signal){
	    	if(PLAYER.getLifePoints() <= 0)
	    		signal = "MODEL:GAME_OVER";
	    		
		    for (IObserver observer : this.observers) {
		        observer.update(signal);
		    }
    	}

	///////////////////////////////////////////////////////////////////////////
    // API pour les actions du joueur
    ///////////////////////////////////////////////////////////////////////////

	    public boolean playerMove(String destination){
	    	TravelPoint tp = this.playerLocation.getTravelPoint(destination);
		    boolean moved = this.PLAYER.travel(tp);
		    if(moved){
			    this.updatePlayerLocation(tp.toPlace());
                this.setEntity(tp.toPlace());
			    this.notifyAllObservers("MODEL:PLAYER_MOVED");
		    }	
		    return moved;
	    }

        public boolean playerUseItemFromInventory(String itemID){
            boolean found = false;
            Item item = this.PLAYER.getItem(itemID);
            if(item != null){
                PLAYER.useItem(PLAYER,item);
                this.notifyAllObservers
                    ("MODEL:PLAYER_USED_ITEM:"+item.getId());
                found = true;
            }
            return found;
        }

        public boolean playerUseItemFromPlace(String itemID){
            boolean found = false;
            Item item = this.playerLocation.getItem(itemID);
            if(item != null){
                PLAYER.useItem(this.playerLocation,item);
                this.notifyAllObservers
                    ("MODEL:PLAYER_USED_ITEM_FROM_PLACE:" + item.getId() );
                found = true;
            }
            return found;
        }

        public boolean playerTakeItemFromPlace(String itemID){
            boolean took = false;
            boolean container = false;
            Item item = this.playerLocation.getItem(itemID);
            
            if(item == null){
                if(this.getEntity().getClassId().equals("CONTAINER")){
                    item = ((Container)this.getEntity()).getItem(itemID);
                    container = true;
                }
            }
            if(item != null){
                took = PLAYER.takeItem
                    (container ? (Container)this.getEntity() 
                               : this.playerLocation,item);
                if(took) 
                    this.notifyAllObservers
                        ("MODEL:PLAYER_TOOK_ITEM_FROM_PLACE:"+item.getId());
            }
            return took;
        }

        public boolean playerTalk(String characterId, String keyword){
            boolean talked = false;
            keyword = keyword.toUpperCase();
            Character c = this.playerLocation.getCharacter(characterId);
            if(c != null){
                String lineId = PLAYER.talk(c,keyword);
                this.notifyAllObservers("MODEL:PLAYER_TALKED:"+lineId);
                talked = true;
            }
            return talked;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Getters et Setters
    ///////////////////////////////////////////////////////////////////////////

	
	public Player getPlayer(){
		return this.PLAYER;
	}
	
	public Place getPlayerLocation(){
		return this.playerLocation;
	}
	
    private void updatePlayerLocation(Place place){
        this.playerLocation = place;
    }
    

	public void forcePlayerLocation(Place place){
		if(place != null){
            if(playerLocation != null){
                playerLocation.delCharacter(PLAYER);
            }
            this.playerLocation = place;
            this.playerLocation.addCharacter(PLAYER);
            this.setEntity(place);
        }
	}
	
	public Entity getEntity(){
    	return this.entity;
    }

    public Event getLastEvent(){
        return this.lastEvent;
    }

    public void setLastEvent(Event e){
        this.lastEvent = e;
    }
	
    public void setEntity(Entity newentity){
	    this.entity = newentity;
    }
}

