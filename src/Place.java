import java.util.*;

public class Place extends Entity
                   implements IInventory {

	private static final String CLASS_ID = "PLACE";
	
    private List<TravelPoint> travelPoints;
    private List<Item> inventory;

    private Map<String, Character> characters;

    public Place(String id){
        super(CLASS_ID, id);
        this.travelPoints = new ArrayList<TravelPoint>();
        this.inventory = new ArrayList<Item>();
        this.characters = new HashMap<String, Character>();
    }

    public TravelPoint[] getTravelPoints(){
        return (TravelPoint[])travelPoints.toArray();
    }

    public void addTravelPoint(TravelPoint tp){
        this.travelPoints.add(tp);
    }

    public TravelPoint getTravelPoint(String id){
        TravelPoint result = null;
        for(TravelPoint tp : travelPoints){
            if(tp.getId().equals(id)){
                result = tp;
            }
        }
        return result;
    }

    public Character[] getCharacters(){
        return (Character[])characters.values().toArray();
    }

    public void addCharacter(Character c){
        this.characters.put(c.getId(),c);
    }
    
    public void delCharacter(Character c){
        this.characters.remove(c.getId());
    }

    public boolean hasCharacter(Character c){
        return this.characters.containsKey(c.getId());
    }
    
    public boolean hasCharacter(String characterID){
        return this.characters.containsKey(characterID);
    }

    public Character getCharacter(String characterID){
        return this.characters.get(characterID);
    }

    public boolean hasItem(Item toSearch){
        boolean result = false;
        Iterator<Item> i = this.inventory.iterator();
        while (!result && i.hasNext() ){
            Item item = i.next();
            if(item.getId() == toSearch.getId()){
                result = true;
            }
        }
        return result;
    }

    public boolean hasItem(String itemID){
        boolean result = false;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = true;
            }
        }
        return result;
    }

    public Item getItem(String itemID){
        Item result = null;
        for(Item i : this.inventory){
            if(itemID.equals(i.getId() )){
                result = i;
            }
        }
        return result;
    }

    public void addItem(Item toAdd){
        this.inventory.add(toAdd);
    }

    public boolean delItem(Item item){
        boolean found = false;
        Iterator<Item> i = this.inventory.iterator();
        while ( !found && i.hasNext() ){
            Item item2 = i.next();
            if( item2 == item ){
                i.remove();
                found = true;
            }
        }
        return found;
    }

    public void putItem(Item i, IInventory to){
        if( this.delItem(i) ) to.addItem(i);
    }

    public Item[] getItems(){
        Item[] tab = new Item[this.inventory.size()];
        tab = this.inventory.toArray(tab);
        return tab;
    }
}
