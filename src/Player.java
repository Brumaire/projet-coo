
public class Player extends Character
                    implements ISingleton{

    private static final Player SELF = new Player("PLAYER");

    public static Player getInstance(){
        return Player.SELF;
    }

	public Player(String id) {
		super(id);
	}
	
	public Player(String id, int friendship) {
		super(id, friendship);
	}
	
	public Player(String id,  int friendship, int life) {
		super(id, friendship, life);
	}
	
}
