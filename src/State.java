/**
 * Implémentation des états.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 **/
public class State implements IState {
    private String id;
    private String classId;

    public State(String classId, String id ){
        this.id = id;
        this.classId = classId;
    }

    public String getId(){
        return this.id;
    }
    
    public String getClassId(){
        return this.classId;
    }

}
