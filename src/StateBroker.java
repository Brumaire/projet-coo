import java.util.*;

public class StateBroker implements IStateBroker,
                                    ISingleton {
    ///////////////////////////////////////////////////////////////////////////
    // Variables de classe
    ///////////////////////////////////////////////////////////////////////////
        private static final StateBroker SELF = new StateBroker();

    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////

        private final Map<String,State> states_collection;

    ///////////////////////////////////////////////////////////////////////////
    // Constructeur
    ///////////////////////////////////////////////////////////////////////////

        private StateBroker(){
            this.states_collection = new HashMap<String, State>();
        }

    ///////////////////////////////////////////////////////////////////////////
    // Implémentation de l'interface statique
    ///////////////////////////////////////////////////////////////////////////
        
        public static StateBroker getInstance(){
            return StateBroker.SELF;
        }
    ///////////////////////////////////////////////////////////////////////////
    // Implémentation de l'interface objets
    ///////////////////////////////////////////////////////////////////////////
 
        public boolean addState(String stateID, String stateClass){
            boolean result = false;
            HashMap<String,State> col = 
                (HashMap<String,State>)this.states_collection;
            
            // Si l'état n'existe pas encore.
            if( ! col.containsKey(stateID)){
                // Le créer
                col.put(stateID,StateFactory.getInstance().build(stateClass, stateID));
                result = true;
            }
            return result;
        }

        public State getState(String stateID){
            return ((HashMap<String,State>)(this.states_collection)).get(stateID);
        }
}
