
/**
 * Cette Factory existe pour les besoins d'évolution du projet, aka si jamais on
 * décide qu'on a besoin de classes filles de State plus tard, pour assurer le
 * polymorphisme.
 * 
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/

public class StateFactory implements ISingleton,
                                     IFactory {
    ///////////////////////////////////////////////////////////////////////////
    // Constantes de classes
    ///////////////////////////////////////////////////////////////////////////

        private static final StateFactory SELF = new StateFactory();

    ///////////////////////////////////////////////////////////////////////////
    // Constructeur
    ///////////////////////////////////////////////////////////////////////////

        public StateFactory(){

        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes statiques
    ///////////////////////////////////////////////////////////////////////////

        public static StateFactory getInstance(){
            return StateFactory.SELF;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes publiques
    ///////////////////////////////////////////////////////////////////////////

        public State build(String stateClass, String stateID){
            State result;
            switch(stateClass){
                case "LINE":
                    result = new Line(stateClass, stateID);
                    break;
                default:
                    result = new State(stateClass, stateID);
            }
            return result;
        }

}
