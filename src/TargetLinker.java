import java.util.HashMap;
import java.util.Map;


public class TargetLinker {

    public static final TargetLinker SELF = new TargetLinker();
    public static TargetLinker getInstance(){
        return TargetLinker.SELF;
    }

    private Map<String,String> targetMap;

    public TargetLinker(){
        this.targetMap = new HashMap<String,String>();
    }


    /*
     * Une target est un argument passé à une commande qui désigne une entité du
     * jeu.
     *
     *
     */
    public String getTarget(String target){
        return this.targetMap.containsKey(target) 
               ? this.targetMap.get(target) 
               : "";
    }

    public void addTarget(String key, String value){
        this.targetMap.put(key,value);
    }

}
