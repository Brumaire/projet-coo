import java.io.*;
import java.util.*;
import org.json.*;

/**
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/
public class TextBroker /* implements ITextBroker */ {
    
    ///////////////////////////////////////////////////////////////////////////
    // Constantes de classes
    ///////////////////////////////////////////////////////////////////////////

        public static final TextBroker SELF = new TextBroker();
        public static final TargetLinker TL = TargetLinker.getInstance();

    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////
 
        private Map<String, String> textMap;

    ///////////////////////////////////////////////////////////////////////////
    // Constructeurs
    ///////////////////////////////////////////////////////////////////////////

        public TextBroker(){
            textMap = new HashMap<String,String>();
        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes statiques
    ///////////////////////////////////////////////////////////////////////////

        public static TextBroker getInstance(){
            return TextBroker.SELF;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes publiques (interface objet)
    ///////////////////////////////////////////////////////////////////////////
        
        // TODO add exceptions support
        public void loadTextJSONFile(String relFilePath) {
            try {
                StringBuilder sb = new StringBuilder();
                BufferedReader br = null;
                try {
                
                    FileInputStream fp = new FileInputStream(relFilePath);
                    InputStreamReader r = new InputStreamReader(fp);
                    br = new BufferedReader(r);
                    String line = br.readLine();
                    while (line != null){
                        sb.append(line);
                        sb.append("\n");
                        line = br.readLine();
                    }
                }
                catch(IOException e){
                    br.close();
                    System.exit(1);
                }
                finally {
                    br.close();
                }
                try {
                    JSONObject json = new JSONObject(sb.toString());
                    String[] keys = json.getNames(json);
                    for( int i = 0; i < keys.length; i++){
                        String key = keys[i];
                        String value = json.getString(key);
                        if(key.startsWith("TARGET_")){
                            TL.addTarget(key.substring(7),value);
                        }
                        else this.textMap.put(key,value);
                    }
                }
                finally {}
            }
            catch(IOException e){
                System.exit(1);
            }
        }

        public void addText(String textID,String text){
            this.textMap.put(textID,text);
        }

        public String getText(String textID){
            String res = this.textMap.get(textID);
            return res == null ? "" : res;
        }
}
