import java.util.*;

public class TravelPoint extends Entity
                         implements ITravelPoint {
             
    private static final String classId = "TRAVELPOINT";            
	private final Place origin, destination;
	private Event event;
	
	public Place fromPlace() {
		return this.origin;
	}
	
	public Place toPlace() {
		return this.destination;
	}

    public void onTravel(Character c){
        if(this.event != null) {
            this.event.addTarget("mainTarget",c);
            this.event.run();
        }
    }
	
	public TravelPoint(String id, Place origin, Place destination){
	    super("TRAVELPOINT", id);	
		if(origin == null)
			throw new IllegalArgumentException("TravelPoint build impossible, origin does not exit.");
		if(destination == null)
			throw new IllegalArgumentException("TravelPoint build impossible, destination does not exit.");
		
		this.origin = origin;
		this.destination = destination;

        this.origin.addTravelPoint(this);
	}
	
	public TravelPoint(String id, Place origin, Place destination, Event event){
		super("TRAVELPOINT",id);
		if(origin == null)
			throw new IllegalArgumentException("TravelPoint build impossible, origin does not exit.");
		if(destination == null)
			throw new IllegalArgumentException("TravelPoint build impossible, destination does not exit.");
		
		this.origin = origin;
		this.destination = destination;
        this.origin.addTravelPoint(this);
		setTravelEvent(event);
	}

    public boolean setTravelEvent(Event e){
		this.event = e;
        return true; // TODO changer ça
    }
}
