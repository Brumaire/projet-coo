import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class View implements IObserver{

    ///////////////////////////////////////////////////////////////////////////
    // Singleton
    ///////////////////////////////////////////////////////////////////////////

        private static final View SELF = new View(Model.getInstance());

        public static View getInstance(){
            return View.SELF;
        }

    ///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////

	    private final Model model;

        private Map<String,DisplayStrategy> strategies;
        
        private String sigArg;

	///////////////////////////////////////////////////////////////////////////
    // Constructeurs
    ///////////////////////////////////////////////////////////////////////////


        public View(Model model){
		    this.model = model;
		    this.model.addObserver(this);

            this.strategies = new HashMap<String,DisplayStrategy>();

            this.addStrategies();
	    }
	

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes privées
    ///////////////////////////////////////////////////////////////////////////

        private void addStrategies(){
            //Afficher l'intro au démarrage du jeu.
            this.strategies.put("MAIN:INTRO",
                    (DisplayStrategy) new DisplayIntro());
            //Survenue d'un évènement affichable
            this.strategies.put("EVENT:TRIGGERED", 
                    (DisplayStrategy) new DisplayEvent());
            //Actualisations du modèles
            this.strategies.put("MODEL:PLAYER_MOVED", 
                    (DisplayStrategy) new DisplayEntity());
            this.strategies.put("MODEL:PLAYER_TALKED",
                    (DisplayStrategy) new DisplayTalk());
            this.strategies.put("MODEL:PLAYER_TOOK_ITEM_FROM_PLACE",
                    (DisplayStrategy) new DisplayTookItem());
            this.strategies.put("MODEL:GAME_OVER",
                    (DisplayStrategy) new DisplayGameOver());
            //Reqûetes directes du contrôleur
            this.strategies.put("CONTROLLER:UNKNOWN_COMMAND",
                    (DisplayStrategy) new DisplayUnknownCommand());
            this.strategies.put("CONTROLLER:BAD_ARGS",
                    (DisplayStrategy) new DisplayBadArgs());
            this.strategies.put("CONTROLLER:HELP", 
                    (DisplayStrategy) new DisplayHelp());
            this.strategies.put("CONTROLLER:QUIT", 
                    (DisplayStrategy) new DisplayQuit());
            this.strategies.put("CONTROLLER:INSPECT", 
                    (DisplayStrategy) new DisplayInspect());
            this.strategies.put("CONTROLLER:SHOW_PLAYER_INVENTORY",
                    (DisplayStrategy) new DisplayPlayerInventory());
            this.strategies.put("CONTROLLER:SHOW_PLAYER_LIFEPOINTS",
                    (DisplayStrategy) new DisplayLifePoints());                   
            //Retour a la vue precedente
            this.strategies.put("CONTROLLER:PREVIOUS_VIEW", 
            		(DisplayStrategy) new DisplayPlace());
        }

    ///////////////////////////////////////////////////////////////////////////
    // Implémentation de IObserver
    ///////////////////////////////////////////////////////////////////////////

        public void update(String signal){
            String[] sigtab = signal.split(":");
            String sigpur = sigtab[0] + ":" + sigtab[1];
            if(sigtab.length > 2){
                this.sigArg = sigtab[2];
            }
            else {
                this.sigArg = null;
            }

            this.updateScreen
                (this.strategies.get(sigpur));
        }
   
        
	///////////////////////////////////////////////////////////////////////////
    // Mise à jour de l'écran
    ///////////////////////////////////////////////////////////////////////////
	
        public void updateScreen(DisplayStrategy d){
	        if(d == null){
                d = new DisplayEntity();
            }
            d.setSignal(this.sigArg);
            d.execute();
        }
	
	///////////////////////////////////////////////////////////////////////////
    // Getters et setters
    ///////////////////////////////////////////////////////////////////////////
        
}
