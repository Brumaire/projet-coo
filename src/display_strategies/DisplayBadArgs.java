public class DisplayBadArgs extends DisplayStrategy {

    public void execute(){
        this.typewriteln("Cette action est impossible.");
        this.postExecutionHook();
    }

}
