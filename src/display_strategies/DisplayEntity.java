public class DisplayEntity extends DisplayStrategy {

    public DisplayEntity(){};

    protected void PostExecutionHook() {
        this.waitForKey();
    }

    public void execute(){
        this.preExecutionHook();
        this.printEntityDesc();
        this.postExecutionHook();
    }


}
