
public class DisplayEvent extends DisplayStrategy {

	public DisplayEvent(){}

	protected void postExecutionHook() {
		this.waitForKey();
	}

	public void execute(){
		Event toDisplay = Model.getInstance()
		                       .getLastEvent();
		String text = TextBroker.getInstance()
		                        .getText("EVENT_"+toDisplay.getId()+"_ONTRIGGER");
		this.preExecutionHook();
		this.typewriteln(text);
		this.postExecutionHook();
	}
}
