
public class DisplayGameOver extends DisplayStrategy {

    public void execute() {
        this.preExecutionHook();
		this.typewriteln("GAME OVER\n Vous etes mort.");
		this.waitForKey();
		System.exit(0);
    }
}
