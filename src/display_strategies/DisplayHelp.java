public class DisplayHelp extends DisplayStrategy {

    public void execute(){
        this.preExecutionHook();
        this.typewriteln
            (TextBroker.getInstance().getText("HELP"));
        this.postExecutionHook();
    }

}
