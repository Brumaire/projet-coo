public class DisplayInspect extends DisplayInventory {
    private static final Model MODEL = Model.getInstance();
    private Entity entity;

    public DisplayInspect(){
    }

    protected void printInventory(){
        this.printInventoryList((IInventory) entity);
    }

    public void execute(){
        this.entity = MODEL.getEntity();
        String classId = this.entity.getClassId();
        this.preExecutionHook();

        this.printEntityDesc();
        
        switch(classId){
            case "CONTAINER":
                this.typewriteln
                    (TextBroker.getInstance()
                               .getText("INVENTORY_CONTAINER"));

                this.printInventory();
                break;
            case "PLACE":
                this.typewriteln
                    (TextBroker.getInstance()
                               .getText("INVENTORY_PLACE"));
                this.printInventory();
                break;
            default :
                break;
        }
        this.postExecutionHook();
    }

}
