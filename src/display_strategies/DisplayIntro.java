public class DisplayIntro extends DisplayStrategy {

    public DisplayIntro(){};

    protected void PostExecutionHook() {
        this.waitForKey();
    }

    public void execute(){
        String text = TextBroker.getInstance()
                                .getText("INTRO_TEXT");
        this.preExecutionHook();
        typewrite(text+"\n\n");
        this.printEntityDesc();


        this.postExecutionHook();
    }


}
