public abstract class DisplayInventory
                extends DisplayStrategy {

        protected abstract void printInventory();

        protected void printInventoryList(IInventory i){
            this.typewriteln(TextBroker.getInstance().getText("INVENTORY_SCREEN_TITLE"));

		    Item[] inventory = i.getItems(); 
		    if(inventory.length == 0) {
			    this.typewriteln(
                        TextBroker.getInstance().getText
                            ("INVENTORY_EMPTY"));
		    }
		    else {
			    String contenir = "";
			    for (Item item : i.getItems()){
				    contenir += TextBroker.getInstance()
                                .getText("ITEM_"+item.getId()+"_NAME");
                    contenir += "\n";
			    }
			    this.typewriteln(contenir);
		    }
	    }

}


