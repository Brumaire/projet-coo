public abstract class DisplayItemInteraction extends DisplayStrategy {
   
	@Override
	protected void postExecutionHook() {
    this.waitForKey();
    View.getInstance().update("CONTROLLER:PREVIOUS_VIEW");
	}
}
