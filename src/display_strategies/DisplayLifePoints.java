public class DisplayLifePoints extends DisplayStrategy {
    
    public DisplayLifePoints(){
    }

	@Override
	protected void postExecutionHook() {
    this.waitForKey();
    View.getInstance().update("CONTROLLER:PREVIOUS_VIEW");
	}
	
    public void execute(){
        this.preExecutionHook();
        this.typewriteln("Vous avez " +
        					Player.getInstance().getLifePoints() +
        					" points de vie.");
        this.postExecutionHook();
    }
}
