public class DisplayPlace extends DisplayStrategy {

    public DisplayPlace(){};

    protected void PostExecutionHook() {
        this.waitForKey();
    }

	protected void printEntityDesc(){
        Entity e = Model.getInstance().getPlayerLocation();
        String textID =
            e.getClassId()+"_"+e.getId()+"_DESC";
        this.typewriteln
            (TextBroker.getInstance().getText(textID));
        Model.getInstance().setEntity(e);
    }
    public void execute(){
        this.preExecutionHook();
        this.printEntityDesc();
        this.postExecutionHook();
    }


}
