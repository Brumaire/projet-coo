public class DisplayPlaceInventory extends DisplayInventory {
    
    private Place playerLocation = Model.getInstance().getPlayerLocation();

    public DisplayPlaceInventory(){
    }

    protected void printInventory(){
        this.printInventoryList(this.playerLocation);
    }

    public void execute(){
        this.preExecutionHook();
        TextBroker.getInstance().getText("INVENTORY_PLACE");
        this.printInventory();
        this.postExecutionHook();
    }

}
