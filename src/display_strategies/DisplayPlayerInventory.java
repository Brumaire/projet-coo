public class DisplayPlayerInventory extends DisplayInventory {

    private Player player = Model.getInstance().getPlayer();

    public DisplayPlayerInventory(){
        this.player = Model.getInstance().getPlayer();
    }

    protected void printInventory(){
        this.printInventoryList(this.player);
    }

    public void execute(){
        this.preExecutionHook();
        TextBroker.getInstance().getText("INVENTORY_PLAYER");
        this.printInventory();
        this.postExecutionHook();
    }

}
