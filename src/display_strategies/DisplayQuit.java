import java.util.Scanner;

public class DisplayQuit extends DisplayStrategy {

    public void execute(){
        this.preExecutionHook();
        
        Scanner sc = new Scanner(System.in);
		String confirmation;
		do {
			this.typewriteln("Sur de vouloir quitter le jeu?(Oui/Non)");
			confirmation = CommandParser.commandNameStandardization(sc.nextLine());
		} while(!confirmation.equals("oui") && 
                !confirmation.equals("non") );
		
		if (confirmation.equals("oui"))
			System.exit(0);
		else
			View.getInstance().update("CONTROLLER:PREVIOUS_VIEW");
    }

}
