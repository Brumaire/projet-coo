import java.io.*;

public abstract class DisplayStrategy implements IDisplayStrategy,
                                                 ISingleton {

    ///////////////////////////////////////////////////////////////////////////
    // Constantes
    ///////////////////////////////////////////////////////////////////////////
    
        public static final long SPEED_TEXT = 8;

	///////////////////////////////////////////////////////////////////////////
    // Attributs
    ///////////////////////////////////////////////////////////////////////////
		
        private String sigArg;

    ///////////////////////////////////////////////////////////////////////////
    // Constructeurs
    ///////////////////////////////////////////////////////////////////////////
    
        public DisplayStrategy(){}

    ///////////////////////////////////////////////////////////////////////////
    // Stratégie d'exécution
    ///////////////////////////////////////////////////////////////////////////


        protected void preExecutionHook(){
            try{
                String operatingSystem = 
                    System.getProperty("os.name");
			   if (operatingSystem.contains("Windows")) {
				new ProcessBuilder("cmd","/c","cls").inheritIO().start().waitFor();
			    }
			    else {
				    new ProcessBuilder("clear").inheritIO().start().waitFor();
			    }
            }
            catch(Exception e){}
        }

        protected void postExecutionHook(){
            this.printPrompt();
        }

        public void setSignal(String sigArg){
            this.sigArg = sigArg;
        }

        public String getSignal(){
            return this.sigArg;
        }

        public abstract void execute();

    ///////////////////////////////////////////////////////////////////////////
    // Méthodes d'affichages génériques
    ///////////////////////////////////////////////////////////////////////////
 
        protected void typewrite(String towrite){
            char[] seq = towrite.toCharArray();
            for(int c = 0 ; c < seq.length ; c++){
                System.out.print(seq[c]);
                try {
                Thread.sleep(SPEED_TEXT);
                }
                catch(InterruptedException ie){}
            }
        }

        protected void typewriteln(String towrite){
            this.typewrite(towrite);
            System.out.print("\n");
        }


        protected void printEntityName(){
            Entity e = Model.getInstance().getEntity();
            String textID =
                e.getClassId()+"_"+e.getId()+"_NAME";
            this.typewriteln
                (TextBroker.getInstance().getText(textID));
        }

        protected void printEntityDesc(){
            Entity e = Model.getInstance().getEntity();
            String textID =
                e.getClassId()+"_"+e.getId()+"_DESC";
            this.typewriteln
                (TextBroker.getInstance().getText(textID));
        }

        protected void printPrompt(){
            this.typewriteln("\n");
            this.typewrite(">>> ");
        }
        
        public void waitForKey(){
			this.typewriteln("Appuyez sur Entrée pour continuer.");
			Controller.getInstance().waitKey();
		}
}
