public class DisplayTalk extends DisplayStrategy {
    public void execute(){
        this.preExecutionHook();
        String line = TextBroker.getInstance().getText(this.getSignal());
        this.typewriteln("« " + line + " »");
        this.postExecutionHook();
    }


}
