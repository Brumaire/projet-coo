public class DisplayTookItem extends DisplayItemInteraction {
    
    public DisplayTookItem(){
    }

    public void execute(){
        this.preExecutionHook();
        String i = TextBroker.getInstance().getText("ITEM_" +
        											this.getSignal() +
        											"_NAME");
        this.typewriteln("Vous avez ramassé " +
        					i + "!");
        this.postExecutionHook();
    }
}
