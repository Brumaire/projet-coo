public class DisplayUnknownCommand extends DisplayStrategy {

    public void execute(){
        this.typewriteln("Commande inconnue, pour avoir de l'aide tapez aide");
        this.postExecutionHook();
    }

}
