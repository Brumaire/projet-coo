/**
 * Interface des personnages.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/
public interface ICharacter {

    /**
     * Méthode permettant à un personnage de voyager (changer de lieu)
     **/
    public abstract boolean travel(TravelPoint tp);

    /**
     * Méthode permettant à un personnage de prendre un objet (dans le lieu ou
     * il se trouve).
     *
     * @return true si l'item a été pris, false sinon.
     **/
    public abstract boolean takeItem(IInventory from, Item i);

    /**
     * Méthode permettant à un personnage d'utiliser un objet
     *
     * @param from l'inventaire ou se trouve l'objet, peut-ềtre le sien, dans ce
     * cas on passera this comme argument.
     *
     * @return true si l'item a bien été utilisé, false sinon
     **/
    public abstract boolean useItem(IInventory from, Item i);

    /**
     * Méthode permettant d'ajouter une ligne de dialogue au personnage.
     **/
    public abstract int addLine(String lineId);

    /**
     * Méthode permettant de retirer une ligne de dialogue au personnage.
     **/
    public abstract int delLine(String lineId);

    /**
     * Méthode permettant de parler à un personnage
     **/
    public abstract String talk(Character c, String lineId);

}
