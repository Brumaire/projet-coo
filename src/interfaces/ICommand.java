
/**
 * Interface pour les commandes du controleur
 **/
public interface ICommand {
    
    /** 
     * Méthode permettantd'executer la commande
     **/
    public abstract boolean  execute();
}
