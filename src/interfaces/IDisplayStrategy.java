
/**
 * Interface des stratégies d'affichage de la vue.
 * Chaque stratégie correspond à un écran du jeu.
 **/
public interface IDisplayStrategy {
    /**
     * Méthode d'affichage à l'écran.
     **/
    public abstract void execute();
}
