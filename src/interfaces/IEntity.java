
/**
 * Interface des Entités du jeu.
 *
 * Les Entités désignent tout ce qui peut intéragir et être sujets à
 * interaction (lieux, personnages, objets).
 *
 * Elles ne sont pas directement 
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 * @version 0.1
 **/
public interface IEntity {
    /**
     * Méthode permettant de savoir si une Entité possède un état donné.
     *
     * @param stateID Chaîne de caractère correspondant à l'ID de l'état.
     *
     * @return true si l'état est présent, false sinon.
     **/
    public abstract boolean hasState(String stateID);

    /**
     * Méthode permettant d'affecter un état à l'entité.
     *
     * @param stateID Chaîne de caractère correspondant à l'ID de l'état.
     *
     * @return  0 si l'état est correctement affecté.
     *         -1 si l'état existe déja dans l'entité.
     *         -2 si l'état n'existe pas.
     **/
    public abstract int addState(String stateId);

    /**
     * Méthode permettant de retirer un état à l'entité.
     *
     * @param stateID Chaîne de caractère correspondant à l'ID de l'état.
     *
     * @return  0 si l'état est correctement supprimé.
     *         -1 si l'état est absent de l'entité.
     *         -2 si l'état n'existe pas.
     **/
    public abstract int delState(String stateId);

    /**
     * Méthode permettant de récupérer l'ID interne de l'entité.
     *
     * Cet ID est utile entre autre pour récupérer le texte lié aux description
     * de cette entité.
     *
     * @return Id de l'entité.
     **/
    public abstract String getId();

    /**
     * Méthode permettant de récupérer la classe de l'entité.
     *
     * Cette classe est utile entre autre pour récupérer le texte lié aux
     * descriptions de cette entité.
     *
     * @return classId de l'entité.
     **/
    public abstract String getClassId();
}
