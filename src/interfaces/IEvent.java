import java.util.*;

/**
 * Les évènements sont des actions se déroulant en réaction à ce que fait le
 * joueur (se déplacer, consommer un item, parler à un personnage etc).
 *
 *
 * Ils fonctionnent avec une méthode onEvent(), celle-ci, quand elle est
 * redéfinie par des classes anonymes héritières, permet de dicter ce qui se produit durant l'évènement. Quand un évènement doit être exécuté, on appelle la méthode run()
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 **/

public interface IEvent {

    /**
     * Cette méthode permet de définir les cibles d'un évènement.
     *
     **/
    public abstract void setTargets(Map<String,Entity> targets);

    /**
     *
     **/
    public abstract void addTarget(String id, Entity target);

    /**
     * Cette méthode permet d'accéder à une cible en particulier de l'évènement.
     **/
    public abstract Entity getTarget(String id);

    /**
     * Ce qui se passe durant l'évềnement
     **/
    public abstract void onEvent();
    
    /** 
     * méthode permettant de lancer un évènement.
     **/
    public abstract void run();

}
