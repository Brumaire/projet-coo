/**
 * Les classes qui implémentent cette interface sont les classes qui peuvent
 * contenir des objets, c'est à dire :
 *
 * - Les personnages, le joueur en particulier
 * - Les objets container
 * - Les lieux (éléments du décor interactibles).
 *
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/

public interface IInventory {
    
    /**
     * Méthode permettant de savoir si l'inventaire de l'entité contient un item
     * en particulier ou non.
     *
     **/
    public abstract boolean hasItem(Item i);

    public abstract boolean hasItem(String itemID);

    public abstract Item getItem(String itemID);

    /**
     * Méthode permettant de retirer un item de son inventaire pour le rajouter
     * dans l'inventaire d'une autre entité.
     **/
    public abstract void putItem(Item i, IInventory to);

    /**
     * Méthode permettant de récupérer un tableau des items contenus dans
     * l'inventaire de l'entité.
     **/
    public abstract Item[] getItems();

    /**
     * Méthode permettant d'ajouter un item dans l'inventaire.
     **/
    public abstract void addItem(Item i);

    /**
     * Méthode permettant de retirer un item de l'inventaire.
     *
     * Renvoie true si cela est fait, false si c'est impossible (si l'item n'est
     * pas présent dans l'inventaire, par exemple.
     **/
    public abstract boolean delItem(Item i);



    

}
