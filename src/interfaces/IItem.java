/**
 * Cette interface décrit les objets.
 *
 * Catégories d'objets :
 *  - objets simples, consommables ou non
 *  - props (décors d'un lieu qui ne peuvent pas être pris).
 *  - containers (objets pouvant contenir d'autre objets).
 *
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/

public interface IItem {

    /**
     * Cette méthode retourne la durabilité (nombre d'utilisation restante) d'un
     * objet).
     *
     * Un objet qui a une durabilité de -1 a un nombre infini d'utilisation. Un
     * objet qui a une durabilité de -2 ne peut pas être utilisé.
     *
     * @return la durabilité ( > 0 ) de l'objet.
     **/
    public abstract int getDurability();

    /**
     * Cette méthode retourne true si l'objet est un prop, false sinon.
     * Un prop est tout simplement un objet qui ne peut pas être placé dans
     * l'inventaire d'un personnage ou dans un container.
     * Ex : Un rocher
     **/
    public abstract boolean isProp();

    /**
     * Méthode appelée quand un objet est utilisé.
     **/
    public abstract void onUse(Character user);
    
    /**
     * Méthode procédurale permettant de générer un évènement particulier quand
     * un objet est utilisé.
     **/
    public abstract void onTake(Character taker);


    public abstract boolean setOnUseEvent(Event e);
    public abstract boolean setOnTakeEvent(Event e);
}
