
/**
 * Interface pour les observers du modèle (essentiellement la vue).
 **/
public interface IObserver {
    
    /** 
     * Méthode permettant d'update la vue.
     **/
    public abstract void  update(String signal);
}
