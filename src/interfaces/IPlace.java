/**
 * Interface des lieux.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 * Les lieux sont des entités.
 * Ils sont caractérisés essentiellement par l'existence :
 *  - D'une liste de TravelPoint.
 *      + Les travelpoints sont les entités qui permettent le passage d'un lieu
 *        à un autre.
 *      + Ils peuvent déclencher des actions sur le modèle.
 *  - D'une liste d'Items.
 *      + Normaux
 *      + Des containers pouvant contenir d'autres items quand fouillés.
 *  - D'un ensemble de personnages présents.
 *
 **/
public interface IPlace {

    /**
     * Méthode permettant de récupérer un travel point en particulier du lieu.
     *
     * @return le travel point correspondant ou NULL si l'id spécifié ne
     * correspond à aucun travel point existant;
     **/
    public abstract TravelPoint getTravelPoint(String id);

    /**
     * Méthode permettant de récupérer un array des travelPoints du lieu.
     *
     * @return un array des travelpoints du lieu.
     **/
    public abstract TravelPoint[] getTravelPoints();


    /**
     * Méthode permettant d'ajouter un personnage
     * à un lieu.
     **/
    public abstract void addCharacter(Character c);

    public abstract boolean hasCharacter(Character c);

    public abstract boolean hasCharacter(String characterId);

    public abstract Character getCharacter(String characterId);

    /**
     * Méthode permettant de retirer un personnage d'un lieu.
     **/
    public abstract void delCharacter(Character c);
    
    /**
     * Méthode permettant de récupérer un array des personnages
     * présent dans un lieu.
     **/
     public abstract Character[] getCharacters();
}

