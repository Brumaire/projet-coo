/**
 * Cette interface vide sert juste à spécifier les classes d'objets singleton du
 * programme pour clarifier le code.
 *
 * Les Singleton doivent avoir une constante de classe de leur type nommée SELF
 * référençant l'unique instance de celle classe.
 *
 * Ils doivent aussi posséder une méthode statique getInstance() retournant
 * cette instance.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 **/
public interface ISingleton {

}
