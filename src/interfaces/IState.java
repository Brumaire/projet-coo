/**
 * Interface modélisant les états.
 * Dans notre programme, les états sont supposés être stockés dans des
 * collections composantes des entités.
 *
 * Les états n'ont pas de responsabilité fonctionelle en eux-mêmes. Ce sont les
 * entités qui définissent comment elles se comportent quand elles ont reçu un
 * état en particulier.
 *
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 * @version 0.1
 **/
public interface IState {

    /**
     * Méthode permettant de récupérer l'ID de l'état.
     * Cet ID est utile, comme pour les entités, afin de récupérer les textes
     * liés à cet état.
     *
     * @return ID de l'état.
     **/
    public abstract String getId();

    /**
     * Méthode permettant de récupérer l'ID de classe de l'état.
     * Cet ID est utile afin de récupérér les texts liés a cet état.
     *
     * @return ID de classe de l'état.
     **/
    public abstract String getClassId();
}
