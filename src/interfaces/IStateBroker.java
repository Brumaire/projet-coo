/**
 * Interface modélisant l'échangeur d'état.
 *
 * l'échangeur d'état sera un singleton qui stockera dans une collection
 * l'ensemble des états qu'il sera possible d'affecter dans le jeu.
 * Pour l'essentiel, il sera appellé par les entités quand celle-ci auront
 * besoin d'un nouvel état.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 * @version 0.1
 *
 **/
public interface IStateBroker {

    /**
     * Méthode permettant d'ajouter un état au jeu.
     *
     * @param stateID L'ID de l'état à ajouter.
     * @param stateClass l'ID de classe de l'état à ajouter.
     *
     * @return true si l'état est ajouté, false s'il existait déja.
     **/
    public abstract boolean addState(String stateID, String stateClasse);

    /**
     * Méthode permettant de récupérer un état pour l'assigner à une entité.
     *
     * @return l'état demandé.
     **/
    public abstract State getState(String stateID);

}
