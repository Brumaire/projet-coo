
/**
 * Interface modélisant l'échangeur de texte.
 *
 * L'échangeur de texte est la classe responsable de la distribution des textes
 * sur demande.
 *
 * L'interêt de cette interface et de la classe qui l'implémentera est de
 * pouvoir déléguer la distribution des textes du jeu.
 * Comme cela, ceux-ci seront centralisés et isolées dans des fichiers .JSON qui seront
 * chargés au démarrage du jeu.
 * Cela a pour principal avantage de faciliter leur édition : Il n'y aura qu'une
 * poignée de fichier a édité et on ne sera pas obligé de naviguer entre les
 * différentes classes du programme.
 *
 * Lors de l'exécution du programme, le TextBroker, qui sera un singleton,
 * réalise la distribution de ces chaines sur demandes.
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 * @version 0.1
 **/

public interface ITextBroker {

    /**
     * Méthode permettant de charger un fichier dans le Broker.
     *
     * @param relFilePath chemin relatif du fichier à charger.
     *
     * @exception FileNotFoundException Le fichier n'a pas été trouvé.
     * @exception JsonParseErrorException Erreur lors du chargement du JSON.
     * @exception IOException Autres erreurs d'entrée-sortie.
     * @exception IDCollisionException Deux textes ont le même ID.
     **/
    public abstract void loadTextJSONFile(String relFilePath);

    /**
     * Méthode permettant de récupérer un texte depuis son id.
     *
     * L'ID spécifié doit correspondre à un unique texte. 
     * 
     * TODO : Ecriture d'une charte pour normaliser les ID et éviter les
     * collisions.
     *
     * @param textID ID du texte à récupérer.
     *
     * @return le texte demandé, ou un message générique si celui-ci n'a pas été
     * trouvé.
     **/
    public abstract String getText(String textID);

}
