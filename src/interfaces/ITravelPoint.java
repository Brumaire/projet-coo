/**
 * Interface des travelPoints
 *
 * @author Lucas Vincent lucas.vincent@entalpi.net
 *
 * Les travelPoints sont des entités.
 * Un travelPoint permet au joueur de naviguer entre deux lieux.
 *
 * L'origine et la destination d'un travelPoint sont déterminé à la contruction
 * de celui-ci. Donc les lieux doivent préexister à la création des travelPoints
 *
 * Les TravelPoint peuvent être créees comme des classes anonymes héritant de
 * TravelPoint et implémentant ITravelPoint quand le programme sera plus mature 
 * afin d'override onTravel et d'ajouter des effets.
 *
 **/
public interface ITravelPoint {
    /**
     * Méthode retournant la destination du travelPoint.
     *
     * @return la destination de ce travelPoint.
     **/
    public abstract Place toPlace();

    /**
     * Méthode retournant l'origine du travelPoint
     *
     * @return l'origine de ce travelPoint.
     **/
    public abstract Place fromPlace();

    /**
     * Méthode à effet de bords, qui est appelée quand le joueur
     * traverse un travelPoint pour pouvoir faire subir les conséquences des
     * évènements qui se produisent durant la traversée.
     *
     **/
    public abstract void onTravel(Character c);

    public abstract boolean setTravelEvent(Event e);
}
